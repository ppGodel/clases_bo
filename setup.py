#!/usr/bin/env python3

from setuptools import find_packages, setup
setup(
    name='courses_bo',
    packages=find_packages(include=['courses_bo']),
    version='0.1.0',
    description='My clases bo library',
    author='Jose Hdz',
    license='MIT',
    setup_requires=['pytest-runner'],
    tests_require=['pytest==4.4.1'],
    test_suite='tests',
)
