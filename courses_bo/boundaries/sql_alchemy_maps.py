#!/usr/bin/env python3
from dataclasses import dataclass
from sqlalchemy import (
    Column,
    Integer,
    String,
    Date,
    ForeignKey,
    Table,
    Boolean,
    Numeric,
)
from sqlalchemy_utils import UUIDType
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from courses_bo.entities import Course, Score, Student

Base = declarative_base()


class Course_db(Base):
    __tablename__ = "Courses"
    id = Column(UUIDType(binary=True), primary_key=True)
    period = Column(String)
    school_id = Column(String)

    def __init__(self, course: Course):
        self.id = Course.id
        self.name = Course.name
        self.period = Course.period
        self.school_id = Course.school_id


class Score_db(Base):
    __tablename__ = "Scores"
    id = Column(UUIDType(binary=True), primary_key=True)
    number = Column(Integer)
    description = Column(String)
    type = Column(String)
    date = Column(Date)

    def __init__(self, score: Score):
        self.id = score.id
        self.number = score.number
        self.description = score.description
        self.type = score.type
        self.date = self.date


@dataclass
class Student_db(Base):
    __tablename__ = "Students"
    id = Column(Integer, primary_key=True)
    names = Column(String)
    last_names = Column(String)
    status = Column(String)

    def __init__(self, student: Student):
        self.id = student.id
        self.names = student.names
        self.last_names = student.last_names
        self.status = student.status
