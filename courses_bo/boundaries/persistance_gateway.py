#!/usr/bin/env python3

from abc import ABC
from typing import List
from uuid import UUID
from courses_bo.entities import Course, Student


class PersistanceGateway(ABC):
    def save_course_obj(self, course: Course) -> None:
        raise NotImplementedError("method cannot be called from base class")

    def save_student_list(self, student_obj_list: List[Student]) -> None:
        raise NotImplementedError("method cannot be called from base class")

    def get_course(self, course_id: UUID) -> Course:
        raise NotImplementedError("method cannot be called from base class")

    def save_attendance_course(self, course_with_attendance: Course) -> None:
        raise NotImplementedError("method cannot be called from base class")

    def save_scores_course(self, course_with_score: Course) -> None:
        raise NotImplementedError("method cannot be called from base class")
