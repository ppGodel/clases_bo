#!/usr/bin/env python3

from enum import Enum
from typing import Any, Callable, Dict, List
from dataclasses import dataclass
from uuid import UUID
from _datetime import datetime


class Status(Enum):
    INACTIVE = 0
    ACTIVE = 1


class AttendanceStatus(Enum):
    MISSING = 0
    PRESENT = 1
    LATE = 2


class ScoreType(Enum):
    EXAM = 0
    ONLINE_EXAM = 1
    PROJECT = 2
    PRACTICE = 3
    FINAL = 4


DATE_STR_FORMAT = "%Y-%m-%d"


@dataclass
class Student:
    id: str
    names: str
    last_names: str
    status: Status

    @classmethod
    def create_from_dict(cls, d: Dict[str, Any]) -> "Student":
        def getStatus(value: str):
            try:
                return Status(int(value))
            except ValueError:
                return Status[value]

        return Student(
            id=str(d["id"]),
            names=str(d.get("names", "")),
            last_names=str(d.get("last_names", "")),
            status=getStatus(d.get("status", Status.INACTIVE.value)),
        )


@dataclass
class Score:
    id: UUID
    number: int
    description: str
    type: ScoreType
    date: datetime

    @classmethod
    def create_from_dict(cls, d: Dict[str, Any]) -> "Score":
        def getType(value: str):
            try:
                return ScoreType(int(value))
            except ValueError:
                return ScoreType[value]

        if getType(d.get("type", "0")) in [ScoreType.PRACTICE]:
            return ScorePractice(
                id=UUID(d["id"]),
                number=int(d["number"]),
                description=str(d["description"]),
                type=getType(d.get("type", "0")),
                date=datetime.strptime(d["date"], DATE_STR_FORMAT),
                name=str(d["name"]),
                practice_aliases=d["practice_aliases"],
                file_aliases=d["file_aliases"],
                as_dir=bool(d["as_dir"]),
                start_date=datetime.strptime(d["start_date"], DATE_STR_FORMAT),
                end_date=datetime.strptime(d["end_date"], DATE_STR_FORMAT),
                limit_date=datetime.strptime(d["limit_date"], DATE_STR_FORMAT),
            )
        else:
            return Score(
                id=UUID(d["id"]),
                number=int(d["number"]),
                description=str(d["description"]),
                type=getType(d.get("type", "0")),
                date=datetime.strptime(d["date"], DATE_STR_FORMAT),
            )


@dataclass
class PracticeFile:
    name: str
    deliver_date: datetime
    file_path: str
    file_raw: bytes
    file_info: List[Dict]


@dataclass
class ScorePractice(Score):
    name: str
    practice_aliases: List[str]
    file_aliases: List[str]
    as_dir: bool
    start_date: datetime
    end_date: datetime
    limit_date: datetime

    def score_practice(self, practice_file: PracticeFile) -> int:
        score = 0
        if (
            not practice_file
            or practice_file.deliver_date > self.limit_date
            or practice_file.deliver_date < self.start_date
        ):
            return score
        if (
            self.start_date <= practice_file.deliver_date <= self.end_date
            and len(practice_file.file_info) > 0
        ):
            score = score + 3
        if practice_file and len(practice_file.file_info) > 0:
            score = score + self.score_file(self.name, practice_file)
        return score

    def score_file(self, practice_name: str, practice_file: PracticeFile) -> int:
        return 7


@dataclass
class StudentScore:
    score_id: UUID
    score_value: float

    @classmethod
    def create_from_dict(cls, d: Dict[str, Any]) -> "StudentScore":
        return StudentScore(
            score_id=UUID(d["score_id"]), score_value=float(d["score_value"])
        )


@dataclass
class Attendance:
    date: datetime
    attendance: AttendanceStatus

    @classmethod
    def create_from_dict(cls, d: Dict[str, Any]) -> "Attendance":
        def getAttendanceStatus(value: str):
            try:
                return AttendanceStatus(int(value))
            except ValueError:
                return AttendanceStatus[value]

        return Attendance(
            date=datetime.strptime(d["date"], DATE_STR_FORMAT),
            attendance=getAttendanceStatus(d["attendance"]),
        )


@dataclass
class StudentClassInfo:
    student: Student
    repo_url: str
    scores: List[StudentScore]
    attendance: List[Attendance]

    @classmethod
    def create_from_dict(cls, d: Dict[str, Any]) -> "StudentClassInfo":
        return StudentClassInfo(
            student=Student.create_from_dict(d["student"]),
            repo_url=d.get("repo_url", ""),
            scores=[
                StudentScore.create_from_dict(student_score)
                for student_score in d.get("scores", [])
            ],
            attendance=[
                Attendance.create_from_dict(student_attendance)
                for student_attendance in d.get("attendance", [])
            ],
        )


@dataclass
class Course:
    id: UUID
    name: str
    school_id: str
    period: str
    scores: List[Score]
    students_list_info: List[StudentClassInfo]

    @classmethod
    def create_from_dict(cls, d: Dict[str, Any]) -> "Course":
        return Course(
            id=UUID(d["id"]),
            name=str(d.get("name", "")),
            school_id=str(d.get("school_id", "")),
            period=str(d.get("period", "")),
            scores=[
                Score.create_from_dict(score_dict) for score_dict in d.get("scores", [])
            ],
            students_list_info=[
                StudentClassInfo.create_from_dict(sci_dict)
                for sci_dict in d["students_list_info"]
            ],
        )
