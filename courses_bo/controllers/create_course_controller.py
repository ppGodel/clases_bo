#!/usr/bin/env python3
from uuid import UUID
from courses_bo.boundaries.persistance_gateway import PersistanceGateway
from courses_bo.entities import Course, Student
from typing import Dict, List


def update_course_controller(
    persistance_gateway: PersistanceGateway,
    course: Dict[str, str],
):
    course_obj = create_course_object_from_dict(course)
    persistance_gateway.save_course_obj(course_obj)


def create_course_object_from_dict(course: Dict[str, str]) -> Course:
    return Course.create_from_dict(course)


def create_student_list_from_list_dict(
    student_list: List[Dict[str, str]]
) -> List[Student]:
    return [Student.create_from_dict(s) for s in student_list]


def get_course_controller(persistence_gateway: PersistanceGateway,
                          course_id: UUID):
    return persistence_gateway.get_course(course_id)


def add_attendance_to_course(
    persistence_gatway: PersistanceGateway, course_with_attendance: Dict
):
    course_obj = Course.create_from_dict(course_with_attendance)
    persistence_gatway.save_attendance_course(course_obj)


def add_scores_to_course(
    persistence_gatway: PersistanceGateway, course_with_scores: Dict
):
    course_obj = Course.create_from_dict(course_with_scores)
    persistence_gatway.save_scores_course(course_obj)
