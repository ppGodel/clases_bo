#!/usr/bin/env python3

import unittest
from courses_bo.entities import (
    Attendance,
    AttendanceStatus,
    PracticeFile,
    ScorePractice,
    ScoreType,
    Status,
    Student,
    Course,
    Score,
    StudentClassInfo,
    StudentScore,
)
import uuid
from datetime import datetime


class Test_Entities(unittest.TestCase):
    def test_create_student(self):
        s = Student(
            id="1",
            names="name1_1 name2_1",
            last_names="last_name1_1 lastname1_2",
            status=Status.ACTIVE,
        )
        self.assertIsNotNone(s)

    def test_create_score(self):
        c = Score(
            id=uuid.UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
            number=0,
            description="Primer parcial",
            type=ScoreType.EXAM,
            date=datetime(2021, 6, 26),
        )
        self.assertIsNotNone(c)

    def test_create_score_practice(self):
        c = ScorePractice(
            id=uuid.UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
            number=1,
            description="Practice 1",
            type=ScoreType.PRACTICE,
            date=datetime(2021, 6, 26),
            name="Practice1",
            file_aliases=["P1", "Practice1"],
            practice_aliases=["P1", "Practice1"],
            as_dir=True,
            start_date=datetime(2012, 6, 26),
            end_date=datetime(2021, 7, 3),
            limit_date=datetime(2021, 7, 31),
        )
        self.assertIsNotNone(c)

    def test_score_practice(self):
        c = ScorePractice(
            id=uuid.UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
            number=1,
            description="Practice 1",
            type=ScoreType.PRACTICE,
            date=datetime(2021, 6, 26),
            name="Practice1",
            file_aliases=["P1", "Practice1"],
            practice_aliases=["P1", "Practice1"],
            as_dir=True,
            start_date=datetime(2021, 6, 26),
            end_date=datetime(2021, 7, 3),
            limit_date=datetime(2021, 7, 31),
        )
        score = c.score_practice(
            PracticeFile(
                name="Practice1.txt",
                deliver_date=datetime(2021, 6, 1),
                file_path="/",
                file_raw=str.encode(""),
                file_info=[{}],
            )
        )
        self.assertEqual(score, 0)
        score = c.score_practice(
            PracticeFile(
                name="Practice1.txt",
                deliver_date=datetime(2021, 7, 1),
                file_path="/",
                file_raw=str.encode(""),
                file_info=[{}],
            )
        )
        self.assertEqual(score, 10)
        score = c.score_practice(
            PracticeFile(
                name="Practice1.txt",
                deliver_date=datetime(2021, 7, 7),
                file_path="/",
                file_raw=str.encode(""),
                file_info=[{}],
            )
        )
        self.assertEqual(score, 7)

        score = c.score_practice(
            PracticeFile(
                name="Practice1.txt",
                deliver_date=datetime(2021, 8, 1),
                file_path="/",
                file_raw=str.encode(""),
                file_info=[],
            )
        )
        self.assertEqual(score, 0)

    def test_create_student_score(self):
        c = StudentScore(
            score_id=uuid.UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"), score_value=0
        )
        self.assertIsNotNone(c)

    def test_create_attendance(self):
        c = Attendance(date=datetime.now(), attendance=AttendanceStatus.PRESENT)
        self.assertIsNotNone(c)

    def test_create_student_class_info(self):
        c = StudentClassInfo(
            student=Student(
                id="1",
                names="name1_1 name2_1",
                last_names="last_name1_1 lastname1_2",
                status=Status.ACTIVE,
            ),
            repo_url="",
            scores=[
                StudentScore(
                    score_id=uuid.UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
                    score_value=5,
                )
            ],
            attendance=[
                Attendance(
                    date=datetime(2021, 6, 26), attendance=AttendanceStatus.PRESENT
                )
            ],
        )
        self.assertIsNotNone(c)

    def test_create_course(self):
        c = Course(
            id=uuid.UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
            name="Course 1",
            school_id="C1",
            period="2-21",
            scores=[
                Score(
                    id=uuid.UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
                    number=0,
                    description="Primer parcial",
                    type=ScoreType.EXAM,
                    date=datetime(2021, 6, 26),
                )
            ],
            students_list_info=[
                StudentClassInfo(
                    student=Student(
                        id="1",
                        names="name1_1 name2_1",
                        last_names="last_name1_1 lastname1_2",
                        status=Status.ACTIVE,
                    ),
                    repo_url="",
                    scores=[
                        StudentScore(
                            score_id=uuid.UUID(
                                "{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"
                            ),
                            score_value=5,
                        )
                    ],
                    attendance=[
                        Attendance(
                            date=datetime(2021, 6, 26),
                            attendance=AttendanceStatus.PRESENT,
                        )
                    ],
                )
            ],
        )
        self.assertIsNotNone(c)
