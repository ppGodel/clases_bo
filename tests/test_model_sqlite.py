#!/usr/bin/env python3

from db_persistance.sqlite_persistance import SqlLiteGateway
from sqlalchemy.exc import IntegrityError
from clases_model_db.alchemy_model.sql_alchemy_maps import (
    CourseDbModel,
    DbModelMapper,
    Base,
)
from datetime import datetime
from uuid import UUID
from courses_bo.entities import (
    Attendance,
    AttendanceStatus,
    Course,
    Score,
    ScoreType,
    Status,
    Student,
    StudentClassInfo,
    StudentScore,
)
import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session


class Test_model_sqlite(unittest.TestCase):
    def test_save_Course(self):
        course_obj = Course(
            id=UUID("{c0000000-0000-0000-0000-000000000001}"),
            name="Course 1",
            school_id="C1",
            period="2-21",
            scores=[
                Score(
                    id=UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
                    number=0,
                    description="Primer parcial",
                    type=ScoreType.EXAM,
                    date=datetime(2021, 6, 26),
                )
            ],
            students_list_info=[
                StudentClassInfo(
                    student=Student(
                        id="1",
                        names="name1_1 name2_1",
                        last_names="last_name1_1 lastname1_2",
                        status=Status.ACTIVE,
                    ),
                    repo_url="",
                    scores=[
                        StudentScore(
                            score_id=UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
                            score_value=5,
                        )
                    ],
                    attendance=[
                        Attendance(
                            date=datetime(2021, 6, 26),
                            attendance=AttendanceStatus.PRESENT,
                        )
                    ],
                )
            ],
        )
        expected_course_db_model = CourseDbModel(
            id=UUID("{C0000000-0000-0000-0000-000000000001}"),
            name="Course 1",
            school_id="C1",
            period="2-21",
        )
        gateway = SqlLiteGateway("test.db")
        gateway.save_course_obj(course_obj)
        course_s = gateway.get_course(course_obj.id)
        self.assertEqual(course_s.id, course_obj.id)
