#!/usr/bin/env python3


from typing import Optional
from courses_bo.boundaries.persistance_gateway import PersistanceGateway
from courses_bo.entities import (
    Attendance,
    AttendanceStatus,
    Course,
    Score,
    ScorePractice,
    ScoreType,
    Status,
    Student,
    StudentClassInfo,
    StudentScore,
)
from courses_bo.controllers.create_course_controller import (
    add_attendance_to_course,
    add_scores_to_course,
    create_course_object_from_dict,
    create_student_list_from_list_dict,
    get_course_controller,
    update_course_controller,
)
import unittest
import uuid
from datetime import datetime


courses_dict = {
    uuid.UUID("C0000000-0000-0000-0000-000000000001"): Course(
        id=uuid.UUID("{C0000000-0000-0000-0000-000000000001}"),
        name="Course 1",
        school_id="C1",
        period="21-2",
        scores=[
            Score(
                id=uuid.UUID("{D0000000-0000-0000-0000-000000000001}"),
                number=1,
                description="Score 1",
                type=ScoreType.EXAM,
                date=datetime(2021, 6, 26),
            ),
            ScorePractice(
                id=uuid.UUID("{D0000000-0000-0000-0000-000000000002}"),
                number=2,
                description="Score 2",
                type=ScoreType.PRACTICE,
                date=datetime(2021, 6, 26),
                name="Practice1",
                practice_aliases=["P1", "P[ractice]1"],
                file_aliases=[""],
                as_dir=True,
                start_date=datetime(2021, 6, 26),
                end_date=datetime(2021, 7, 3),
                limit_date=datetime(2021, 7, 31),
            ),
        ],
        students_list_info=[
            StudentClassInfo(
                student=Student(
                    id="1",
                    names="name1_1 name1_2",
                    last_names="last_name1_1 last_name1_2",
                    status=Status.ACTIVE,
                ),
                repo_url="",
                scores=[
                    StudentScore(
                        score_id=uuid.UUID("{D0000000-0000-0000-0000-000000000001}"),
                        score_value=5,
                    )
                ],
                attendance=[
                    Attendance(
                        date=datetime(2021, 6, 26),
                        attendance=AttendanceStatus.PRESENT,
                    )
                ],
            )
        ],
    )
}


class MockPersistanceGateway(PersistanceGateway):
    def __init__(self):
        self.is_course_send_to_persistance = False
        self.is_student_list_sent_to_persistance = False
        self.is_course_attendance_sent_to_persistance = False
        self.is_course_score_sent_to_persistance = False

    def save_course_obj(self, course: Course) -> None:
        self.is_course_send_to_persistance = True

    def save_student_list(self, student_obj_list) -> None:
        self.is_student_list_sent_to_persistance = True

    def get_course(self, course_id: uuid.UUID) -> Optional[Course]:
        return courses_dict.get(course_id, None)

    def save_attendance_course(self, course_with_attendance: Course) -> None:
        self.is_course_attendance_sent_to_persistance = True

    def save_scores_course(self, course_with_score: Course) -> None:
        self.is_course_score_sent_to_persistance = True


class TestCreateCourseController(unittest.TestCase):
    mock_gateway_persistance = MockPersistanceGateway()
    course_dict = {
        "id": "C0000000-0000-0000-0000-000000000001",
        "name": "Course 1",
        "school_id": "C1",
        "period": "21-2",
        "scores": [
            {
                "id": "D0000000-0000-0000-0000-000000000001",
                "number": "1",
                "description": "Score 1",
                "date": "2021-06-26",
                "type": "0",
            },
            {
                "id": "D0000000-0000-0000-0000-000000000002",
                "number": "2",
                "description": "Score 2",
                "date": "2021-06-26",
                "type": "3",
                "name": "Practice1",
                "practice_aliases": ["P1", "P[ractice]1"],
                "file_aliases": [""],
                "as_dir": "True",
                "start_date": "2021-06-26",
                "end_date": "2021-07-03",
                "limit_date": "2021-07-31",
            },
        ],
        "students_list_info": [
            {
                "student": {
                    "id": "1",
                    "names": "name1_1 name1_2",
                    "last_names": "last_name1_1 last_name1_2",
                    "status": "1",
                },
                "scores": [
                    {
                        "score_id": "D0000000-0000-0000-0000-000000000001",
                        "score_value": "5",
                    }
                ],
                "attendance": [{"date": "2021-06-26", "attendance": "1"}],
            }
        ],
    }

    student_dict_list = [
        {
            "id": "1",
            "names": "name1_1 name1_2",
            "last_names": "last_name1_1 last_name1_2",
            "status": "1",
        },
        {
            "id": "2",
            "names": "name2_1 name2_2",
            "last_names": "last_name2_1 last_name2_2",
            "status": "ACTIVE",
        },
        {
            "id": "3",
            "names": "name3_1 name3_2",
            "last_names": "last_name3_1 last_name3_2",
            "status": "0",
        },
    ]

    attendance_course_dict = {
        "id": "C0000000-0000-0000-0000-000000000001",
        "students_list_info": [
            {
                "student": {
                    "id": "1",
                },
                "attendance": [{"date": "2021-06-27", "attendance": "1"}],
            },
            {
                "student": {
                    "id": "2",
                },
                "attendance": [{"date": "2021-06-27", "attendance": "1"}],
            },
            {
                "student": {
                    "id": "3",
                },
                "attendance": [{"date": "2021-06-27", "attendance": "0"}],
            },
        ],
    }

    score_course_dict = {
        "id": "C0000000-0000-0000-0000-000000000001",
        "scores": [
            {
                "id": "D0000000-0000-0000-0000-000000000001",
                "number": "1",
                "description": "Score 1",
                "date": "2021-06-26",
            }
        ],
        "students_list_info": [
            {
                "student": {
                    "id": "1",
                },
                "scores": [
                    {
                        "score_id": "D0000000-0000-0000-0000-000000000001",
                        "score_value": "5",
                    }
                ],
            },
            {
                "student": {
                    "id": "2",
                },
                "scores": [
                    {
                        "score_id": "D0000000-0000-0000-0000-000000000001",
                        "score_value": "7",
                    }
                ],
            },
            {
                "student": {
                    "id": "3",
                },
                "scores": [
                    {
                        "score_id": "D0000000-0000-0000-0000-000000000001",
                        "score_value": "9",
                    }
                ],
            },
        ],
    }

    def test_create_course_obj(self):
        course_obj_created = create_course_object_from_dict(self.course_dict)
        self.assertEqual(
            courses_dict[uuid.UUID("C0000000-0000-0000-0000-000000000001")],
            course_obj_created,
        )

    def test_create_student_list_object(self):
        student_obj_list = [
            Student(
                id="1",
                names="name1_1 name1_2",
                last_names="last_name1_1 last_name1_2",
                status=Status.ACTIVE,
            ),
            Student(
                id="2",
                names="name2_1 name2_2",
                last_names="last_name2_1 last_name2_2",
                status=Status.ACTIVE,
            ),
            Student(
                id="3",
                names="name3_1 name3_2",
                last_names="last_name3_1 last_name3_2",
                status=Status.INACTIVE,
            ),
        ]
        student_obj_created = create_student_list_from_list_dict(self.student_dict_list)
        self.assertEqual(student_obj_created, student_obj_list)

    def test_update_course_controller(self):
        update_course_controller(self.mock_gateway_persistance, self.course_dict)
        self.assertTrue(self.mock_gateway_persistance.is_course_send_to_persistance)

    def test_get_course_controller(self):
        course = get_course_controller(
            self.mock_gateway_persistance,
            uuid.UUID("C0000000-0000-0000-0000-000000000001"),
        )
        self.assertEqual(
            courses_dict[uuid.UUID("C0000000-0000-0000-0000-000000000001")], course
        )

        course = get_course_controller(
            self.mock_gateway_persistance,
            uuid.UUID("C0000000-0000-0000-0000-000000000000"),
        )
        self.assertIsNone(course)

    def test_create_course_attendance(self):
        expected_course = Course(
            id=uuid.UUID("{C0000000-0000-0000-0000-000000000001}"),
            name="",
            school_id="",
            period="",
            scores=[],
            students_list_info=[
                StudentClassInfo(
                    student=Student(
                        id="1",
                        names="",
                        last_names="",
                        status=Status.INACTIVE,
                    ),
                    repo_url="",
                    scores=[],
                    attendance=[
                        Attendance(
                            date=datetime(2021, 6, 27),
                            attendance=AttendanceStatus.PRESENT,
                        )
                    ],
                ),
                StudentClassInfo(
                    student=Student(
                        id="2",
                        names="",
                        last_names="",
                        status=Status.INACTIVE,
                    ),
                    repo_url="",
                    scores=[],
                    attendance=[
                        Attendance(
                            date=datetime(2021, 6, 27),
                            attendance=AttendanceStatus.PRESENT,
                        )
                    ],
                ),
                StudentClassInfo(
                    student=Student(
                        id="3",
                        names="",
                        last_names="",
                        status=Status.INACTIVE,
                    ),
                    repo_url="",
                    scores=[],
                    attendance=[
                        Attendance(
                            date=datetime(2021, 6, 27),
                            attendance=AttendanceStatus.MISSING,
                        )
                    ],
                ),
            ],
        )
        course_object = Course.create_from_dict(self.attendance_course_dict)
        self.assertEqual(expected_course, course_object)

    def test_send_attendance_to_persistance(self):
        add_attendance_to_course(
            self.mock_gateway_persistance, self.attendance_course_dict
        )
        self.assertTrue(
            self.mock_gateway_persistance.is_course_attendance_sent_to_persistance
        )

    def test_add_score_course(self):
        expected_course = Course(
            id=uuid.UUID("{C0000000-0000-0000-0000-000000000001}"),
            name="",
            school_id="",
            period="",
            scores=[
                Score(
                    id=uuid.UUID("{D0000000-0000-0000-0000-000000000001}"),
                    number=1,
                    description="Score 1",
                    type=ScoreType.EXAM,
                    date=datetime(2021, 6, 26),
                )
            ],
            students_list_info=[
                StudentClassInfo(
                    student=Student(
                        id="1",
                        names="",
                        last_names="",
                        status=Status.INACTIVE,
                    ),
                    repo_url="",
                    scores=[
                        StudentScore(
                            score_id=uuid.UUID(
                                "{D0000000-0000-0000-0000-000000000001}"
                            ),
                            score_value=5,
                        )
                    ],
                    attendance=[],
                ),
                StudentClassInfo(
                    student=Student(
                        id="2",
                        names="",
                        last_names="",
                        status=Status.INACTIVE,
                    ),
                    repo_url="",
                    scores=[
                        StudentScore(
                            score_id=uuid.UUID(
                                "{D0000000-0000-0000-0000-000000000001}"
                            ),
                            score_value=7,
                        )
                    ],
                    attendance=[],
                ),
                StudentClassInfo(
                    student=Student(
                        id="3",
                        names="",
                        last_names="",
                        status=Status.INACTIVE,
                    ),
                    repo_url="",
                    scores=[
                        StudentScore(
                            score_id=uuid.UUID(
                                "{D0000000-0000-0000-0000-000000000001}"
                            ),
                            score_value=9,
                        )
                    ],
                    attendance=[],
                ),
            ],
        )
        course_object = Course.create_from_dict(self.score_course_dict)
        self.assertEqual(expected_course, course_object)

    def test_send_score_to_persistance(self):
        add_scores_to_course(self.mock_gateway_persistance, self.score_course_dict)
        self.assertTrue(
            self.mock_gateway_persistance.is_course_score_sent_to_persistance
        )
