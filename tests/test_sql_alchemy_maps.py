#!/usr/bin/env python3
from clases_model_db.alchemy_model.sql_alchemy_maps import (
    CourseDbModel,
    DbModelMapper,
    ScoreDbModel,
    StudentDbModel,
)
from datetime import datetime
from uuid import UUID

from courses_bo.entities import (
    Attendance,
    AttendanceStatus,
    Course,
    Score,
    ScoreType,
    Status,
    Student,
    StudentClassInfo,
    StudentScore,
)
import unittest


class TestModels(unittest.TestCase):
    def test_create_Course(self):
        course_obj = Course(
            id=UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
            name="Course 1",
            school_id="C1",
            period="2-21",
            scores=[
                Score(
                    id=UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
                    number=0,
                    description="Primer parcial",
                    type=ScoreType.EXAM,
                    date=datetime(2021, 6, 26),
                )
            ],
            students_list_info=[
                StudentClassInfo(
                    student=Student(
                        id="1",
                        names="name1_1 name2_1",
                        last_names="last_name1_1 lastname1_2",
                        status=Status.ACTIVE,
                    ),
                    repo_url="",
                    scores=[
                        StudentScore(
                            score_id=UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
                            score_value=5,
                        )
                    ],
                    attendance=[
                        Attendance(
                            date=datetime(2021, 6, 26),
                            attendance=AttendanceStatus.PRESENT,
                        )
                    ],
                )
            ],
        )
        expected_course_db_model = CourseDbModel(
            id=UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
            name="Course 1",
            school_id="C1",
            period="2-21",
        )
        course_db_model = DbModelMapper.map_course(course=course_obj)
        self.assertEqual(expected_course_db_model.id, course_db_model.id)
        self.assertEqual(expected_course_db_model.name, course_db_model.name)
        self.assertEqual(expected_course_db_model.school_id, course_db_model.school_id)
        self.assertEqual(expected_course_db_model.period, course_db_model.period)

    def test_create_Score_model(self):
        score_obj = Score(
            id=UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
            number=0,
            description="Primer parcial",
            type=ScoreType.EXAM,
            date=datetime(2021, 6, 26),
        )
        expected_score_db_model = ScoreDbModel(
            id=UUID("{0b0ca3b2-17f4-4453-8708-d8ca1853d262}"),
            number=0,
            description="Primer parcial",
            type=ScoreType.EXAM,
            date=datetime(2021, 6, 26),
        )
        score_db_model = DbModelMapper.mapScore(score=score_obj)
        self.assertEqual(expected_score_db_model.id, score_db_model.id)
        self.assertEqual(expected_score_db_model.number, score_db_model.number)
        self.assertEqual(
            expected_score_db_model.description, score_db_model.description
        )
        self.assertEqual(expected_score_db_model.type, score_db_model.type)
        self.assertEqual(expected_score_db_model.date, score_db_model.date)

    def test_create_student_model(self):
        student_obj = Student(
            id="1",
            names="name1_1 name2_1",
            last_names="last_name1_1 lastname1_2",
            status=Status.ACTIVE,
        )
        expected_student_db_model = StudentDbModel(
            id="1",
            names="name1_1 name2_1",
            last_names="last_name1_1 lastname1_2",
            status=Status.ACTIVE,
        )
        score_db_model = DbModelMapper.mapStudent(student=student_obj)
        self.assertEqual(expected_student_db_model.id, score_db_model.id)
        self.assertEqual(expected_student_db_model.names, score_db_model.names)
        self.assertEqual(
            expected_student_db_model.last_names, score_db_model.last_names
        )
