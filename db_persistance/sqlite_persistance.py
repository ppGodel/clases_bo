#!/usr/bin/env python3

from sqlalchemy.engine.base import Engine
from sqlalchemy.engine.mock import MockConnection
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.session import sessionmaker
from clases_model_db.alchemy_model.sql_alchemy_maps import (
    Base,
    CourseDbModel,
    DbModelMapper,
)
from typing import Any, Callable, Dict, List, Optional, Union
from uuid import UUID

from sqlalchemy.engine.create import create_engine
from courses_bo.entities import Course, Student
from courses_bo.boundaries.persistance_gateway import PersistanceGateway
import sqlite3


class SqlLiteGateway(PersistanceGateway):
    def __init__(
        self,
        db_name: str,
        engine_creator: Callable[
            [str, Optional[Dict[str, Any]]], Union[Engine, MockConnection]
        ] = create_engine,
    ) -> None:
        engine = engine_creator(f"sqlite:///{db_name}", **{})
        Base.metadata.create_all(engine)
        self.Session = sessionmaker(bind=engine)

    def save_course_obj(self, course: Course) -> None:
        course_db_model = DbModelMapper.map_course(course=course)
        with self.Session() as session:  # type: Session
            session.merge(course_db_model)
            try:
                session.commit()
            except (IntegrityError):
                session.rollback()

    def save_student_list(self, student_obj_list: List[Student]) -> None:
        pass

    def get_course(self, course_id: UUID) -> Course:
        with self.Session() as session:  # type: Session
            course_db_model = (
                session.query(CourseDbModel).filter_by(id=course_id).first()
            )
            return DbModelMapper.map_course_db_model(course_db_model)

    def save_attendance_course(self, course_with_attendance: Course) -> None:
        pass

    def save_scores_course(self, course_with_score: Course) -> None:
        pass
