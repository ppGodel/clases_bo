#!/usr/bin/env python3
from dataclasses import dataclass
from sqlalchemy import (
    Column,
    Integer,
    String,
    Date,
    ForeignKey,
    Table,
    Boolean,
    Numeric,
)
from sqlalchemy_utils import UUIDType
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from courses_bo.entities import Course, Score, Student

Base = declarative_base()


class CourseDbModel(Base):
    __tablename__ = "Courses"
    id = Column(UUIDType(binary=True), primary_key=True)
    name = Column(String)
    period = Column(String)
    school_id = Column(String)


class ScoreDbModel(Base):
    __tablename__ = "Scores"
    id = Column(UUIDType(binary=True), primary_key=True)
    number = Column(Integer)
    description = Column(String)
    type = Column(String)
    date = Column(Date)


class StudentDbModel(Base):
    __tablename__ = "Students"
    id = Column(Integer, primary_key=True)
    names = Column(String)
    last_names = Column(String)
    status = Column(String)


class DbModelMapper:
    @staticmethod
    def map_course(course: Course) -> CourseDbModel:
        return CourseDbModel(
            id=course.id,
            name=course.name,
            period=course.period,
            school_id=course.school_id,
        )

    @staticmethod
    def map_course_db_model(course_db_model: CourseDbModel) -> Course:
        return Course(
            id=course_db_model.id,
            name=course_db_model.name,
            period=course_db_model.period,
            school_id=course_db_model.school_id,
            scores=[],
            students_list_info=[],
        )

    @staticmethod
    def mapScore(score: Score) -> ScoreDbModel:
        return ScoreDbModel(
            id=score.id,
            number=score.number,
            description=score.description,
            type=score.type,
            date=score.date,
        )

    @staticmethod
    def mapStudent(student: Student) -> StudentDbModel:
        return StudentDbModel(
            id=student.id,
            names=student.names,
            last_names=student.last_names,
            status=student.status,
        )
